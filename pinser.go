package main

import (
	"fmt"
	"log"
	"os"
	"path/filepath"
	// "runtime"
	"sync"

	"git.sr.ht/~nihil/pinser/config"
)

type ETag string

type ItemStatus struct {
	TagA  ETag
	HashA string
	TagB  ETag
	HashB string
}

// Maintains necessary info about the sync of A and B. Empty on first sync.
type SyncStatus struct {
	status    map[string]ItemStatus
	newStatus map[string]ItemStatus
}

func LoadSyncStatus(conf *config.PinserConfig, name string, logger *log.Logger) (*SyncStatus, error) {
	path := filepath.Join(conf.General.StatusPath, fmt.Sprintf("%s.json", name))
	logger.Printf("Loading sync status %v\n", path)
	return nil, nil
}

func SaveSyncStatus(pair *config.PairConfig, status *SyncStatus) error {
	return nil
}

// https://unterwaditzer.net/2016/sync-algorithm.html
func Sync(confA config.StorageConfig, confB config.StorageConfig, status *SyncStatus, logger *log.Logger) error {
	storageA, err := confA.Storage()
	if err != nil {
		return err
	}
	storageB, err := confB.Storage()
	if err != nil {
		return err
	}

	mapA := make(map[string]string)
	for it := range storageA.Items() {
		if it.Error != nil {
			// logger.Printf("item error: %s\n", item.Error)
			// continue
			return it.Error
		}
		mapA[it.Item.Path] = it.Item.ETag
	}
	mapB := make(map[string]string)
	for it := range storageB.Items() {
		if it.Error != nil {
			// logger.Printf("item error: %s\n", item.Error)
			// continue
			return it.Error
		}
		mapB[it.Item.Path] = it.Item.ETag
	}
	logger.Printf("Items in A: %v\n", len(mapA))
	logger.Printf("Items in B: %v\n", len(mapB))
	return nil
}

type SyncPairTask struct {
	name string
	pair config.PairConfig
}

type SyncPairResult struct {
	name string
	err  error
}

func SyncPair(conf *config.PinserConfig, wg *sync.WaitGroup, task SyncPairTask, out chan SyncPairResult, logger *log.Logger) {
	defer wg.Done()

	status, err := LoadSyncStatus(conf, task.name, logger)
	if err != nil {
		out <- SyncPairResult{task.name, err}
		return
	}
	logger.Printf("Syncing %s (%s, %s)\n", task.name, task.pair.StorageA, task.pair.StorageB)
	err = Sync(
		conf.Storages[task.pair.StorageA],
		conf.Storages[task.pair.StorageB],
		status,
		logger)
	if err != nil {
		out <- SyncPairResult{task.name, err}
		return
	}
	err = SaveSyncStatus(&task.pair, status)
	if err != nil {
		out <- SyncPairResult{task.name, err}
		return
	}

	out <- SyncPairResult{task.name, nil}
}

func SyncCollections(conf *config.PinserConfig, collections []string, logger *log.Logger) error {
	if len(collections) == 0 {
		// We should sync all pairs
		out := make(chan SyncPairResult)
		wg := &sync.WaitGroup{}
		for name, pair := range conf.Pairs {
			wg.Add(1)
			go SyncPair(conf, wg, SyncPairTask{name, pair}, out, logger)
		}
		go func() {
			wg.Wait()
			close(out)
		}()
		for result := range out {
			if result.err != nil {
				logger.Printf("Error while syncing %q: %v\n", result.name, result.err)
				continue
			}
			logger.Printf("Done syncing %q\n", result.name)
		}
	}
	return nil
}

func main() {
	logger := log.New(os.Stdout, "", log.LstdFlags)
	logger.Println("Starting pinser")

	conf, err := config.LoadConfigFromFile(nil, logger)
	if err != nil {
		logger.Fatalf("Failed to load config: %v\n", err)
	}
	if err := SyncCollections(conf, nil, logger); err != nil {
		logger.Fatalf("Sync failed: %v\n", err)
	}
}
