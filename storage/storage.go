package storage

import (
	"fmt"
	"io"
	// "net/http"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"github.com/emersion/go-webdav"
	"github.com/mitchellh/go-homedir"
)

const (
	version = "0.1"
)

var DefaultRequestHeaders = map[string]string{
	"User-Agent":   fmt.Sprintf("pinser/%s", version),
	"Content-Type": "application/xml; charset=UTF-8",
}

type ItemsResult struct {
	Item  webdav.FileInfo
	Error error
}

type Storage interface {
	Items() chan ItemsResult
}

type CaldavStorage struct {
	*webdav.Client
	path string
}

type CarddavStorage struct {
	Data string
}

type LocalStorage struct {
	*webdav.LocalFileSystem
	FileExt    string
	IgnoredExt string // ".tmp"
	// Collections []string
}

// The password command is assumed to output one single line (possibly with a
// trailing \n) containing the password.
// `args[0]` is the command itself, args[1:] contains its arguments.
func runPasswordCmd(args []string) (string, error) {
	cmd := exec.Command(args[0], args[1:]...)
	output, err := cmd.Output()
	if err != nil {
		return "", err
	}
	password := strings.TrimSuffix(string(output), "\n")
	return password, nil
}

func splitHostPath(url string) (string, string) {
	// Faster and simpler than parsing with url.Parse().
	// NOTE: this assumes 8 is a decent choice to do this search.
	// Hopefully no bugs will bite us in the future.
	pathSlash := strings.Index(url[8:], "/") + 8
	host, path := url[:pathSlash], url[pathSlash:]
	return host, path
}

// Initialize a new CalDAV storage, where `url` contains the full URL to the
// calendar entries themselves, not a collection of calendars.
func NewCaldavStorage(url, username string, passwordCmd []string) (Storage, error) {
	// TODO: for how long we should hold the password in memory?
	password, err := runPasswordCmd(passwordCmd)
	if err != nil {
		return nil, err
	}

	host, path := splitHostPath(url)
	client, err := webdav.NewClient(webdav.HTTPClientWithBasicAuth(
		nil,
		username,
		password,
	), host)
	if err != nil {
		return nil, err
	}
	return CaldavStorage{client, path}, nil
}

func discoverLocalCollections(path string) ([]string, error) {
	// Every directory found at the root of the storage is a collec tion
	path, err := homedir.Expand(path)
	if err != nil {
		return nil, err
	}
	entries, err := os.ReadDir(path)
	if err != nil {
		return nil, err
	}

	// We don't expect huge lists here, right?
	collections := make([]string, 0)
	for _, entry := range entries {
		if !entry.IsDir() {
			continue
		}
		collections = append(collections, entry.Name())
	}

	return collections, nil
}

// CalDAV or CardDAV storage backed by the local filesystem.
func NewLocalStorage(path string, fileExt string, ignoredExt string) (Storage, error) {
	path, err := homedir.Expand(path)
	if err != nil {
		return nil, err
	}
	fileSystem := webdav.LocalFileSystem(path)
	return LocalStorage{&fileSystem, fileExt, ignoredExt}, nil
}

type internalStorage interface {
	Readdir(name string, recursive bool) ([]webdav.FileInfo, error)
	Create(name string) (io.WriteCloser, error)
	RemoveAll(name string) error
	Mkdir(name string) error

	// Which files should be ignored by this storage
	// used by getItems()
	isIgnored(fi *webdav.FileInfo) bool
}

// Works as Readdir() with a channel as output. Filters out stuff on which
// isIgnored() returns false.
func getItems(storage internalStorage, path string, out chan ItemsResult) {
	defer close(out)
	dirInfo, err := storage.Readdir(path, false)
	if err != nil {
		out <- ItemsResult{webdav.FileInfo{}, err}
		return
	}
	for _, fi := range dirInfo {
		if storage.isIgnored(&fi) {
			continue
		}
		fi.Path = filepath.Base(fi.Path)
		out <- ItemsResult{fi, nil}
	}
}

func (storage CaldavStorage) isIgnored(fi *webdav.FileInfo) bool {
	return len(fi.ETag) == 0
}

func (storage CaldavStorage) Items() chan ItemsResult {
	out := make(chan ItemsResult)
	// XXX: log items missing ETag?
	go getItems(storage, storage.path, out)
	return out
}

func (storage CarddavStorage) Items() chan ItemsResult {
	out := make(chan ItemsResult)
	defer close(out)
	return out
}

func (storage LocalStorage) isIgnored(fi *webdav.FileInfo) bool {
	return !strings.HasSuffix(fi.Path, storage.FileExt) ||
		strings.HasSuffix(fi.Path, storage.IgnoredExt) ||
		fi.IsDir
}

func (storage LocalStorage) Items() chan ItemsResult {
	out := make(chan ItemsResult)
	go getItems(storage, "/", out)
	return out
}
