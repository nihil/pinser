module git.sr.ht/~nihil/pinser

go 1.17

replace github.com/emersion/go-webdav => /home/fadel/src/go-webdav

require (
	github.com/emersion/go-webdav v0.3.2-0.20220412073826-25dfbaf95ee6
	github.com/go-ini/ini v1.63.2
	github.com/kyoh86/xdg v1.2.0
	github.com/mitchellh/go-homedir v1.1.0
)

require (
	github.com/emersion/go-ical v0.0.0-20200224201310-cd514449c39e // indirect
	github.com/stretchr/testify v1.7.1 // indirect
)
