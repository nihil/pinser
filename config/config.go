package config

import (
	"fmt"
	"log"
	"os"
	"path"
	"strings"

	"github.com/go-ini/ini"
	"github.com/kyoh86/xdg"

	"git.sr.ht/~nihil/pinser/storage"
)

type ConflictResolutionKind uint32

const (
	ConflictAWins ConflictResolutionKind = iota
	ConflictBWins
)

type GeneralConfig struct {
	StatusPath string `ini:"status_path"`
}

type PairConfig struct {
	StorageA           string   `ini:"a"`
	StorageB           string   `ini:"b"`
	Collections        []string `ini:"collections"`
	ConflictResolution ConflictResolutionKind
}

type CaldavStorageConfig struct {
	Url         string   `ini:"url"`
	Username    string   `ini:"username"`
	PasswordCmd []string `ini:"password.fetch"`
}

type CarddavStorageConfig struct {
	Url         string   `ini:"url"`
	Username    string   `ini:"username"`
	PasswordCmd []string `ini:"password.fetch"`
}

type LocalStorageConfig struct {
	Path    string `ini:"path"`
	FileExt string `ini:"fileext"`
}

type StorageConfig interface {
	Storage() (storage.Storage, error)
}

func (conf CaldavStorageConfig) Storage() (storage.Storage, error) {
	return storage.NewCaldavStorage(conf.Url, conf.Username, conf.PasswordCmd)
}

func (conf CarddavStorageConfig) Storage() (storage.Storage, error) {
	return storage.CarddavStorage{conf.Username}, nil
}

func (conf LocalStorageConfig) Storage() (storage.Storage, error) {
	return storage.NewLocalStorage(conf.Path, conf.FileExt, ".tmp")
}

type PinserConfig struct {
	General  GeneralConfig
	Pairs    map[string]PairConfig
	Storages map[string]StorageConfig
	Ini      *ini.File `ini:"-"`
}

func (conf *PinserConfig) LoadConfig(file *ini.File) error {
	if general, err := file.GetSection("general"); err == nil {
		if err := general.MapTo(&conf.General); err != nil {
			return err
		}
	}

	pairs := make(map[string]PairConfig)
	storages := make(map[string]StorageConfig)
	for _, section := range file.Sections() {
		parts := strings.Split(section.Name(), " ")
		if len(parts) < 2 {
			// Should be ignored
			continue
		} else if len(parts) > 2 {
			return fmt.Errorf("Invalid section name: %s", section.Name())
		}
		prefix := parts[0]
		name := parts[1]
		if prefix == "pair" {
			var pairConfig PairConfig
			if err := section.MapTo(&pairConfig); err != nil {
				return err
			}
			conflictResolutionKind := section.Key("conflict_resolution").String()
			switch conflictResolutionKind {
			case "a wins":
				pairConfig.ConflictResolution = ConflictAWins
			case "b wins":
				pairConfig.ConflictResolution = ConflictBWins
			default:
				return fmt.Errorf("Invalid conflict resolution kind: %s", conflictResolutionKind)
			}
			pairs[name] = pairConfig
		} else if prefix == "local_storage" {
			var storageConfig LocalStorageConfig
			if err := section.MapTo(&storageConfig); err != nil {
				return err
			}
			storages[name] = storageConfig
		} else if prefix == "caldav_storage" {
			var storageConfig CaldavStorageConfig
			if err := section.MapTo(&storageConfig); err != nil {
				return err
			}
			storages[name] = storageConfig
		} else if prefix == "carddav_storage" {
			var storageConfig CarddavStorageConfig
			if err := section.MapTo(&storageConfig); err != nil {
				return err
			}
			storages[name] = storageConfig
		}
	}
	// TODO: ensure pairs are disjoint
	conf.Pairs = pairs
	conf.Storages = storages
	return nil
}

func LoadConfigFromFile(root *string, logger *log.Logger) (*PinserConfig, error) {
	if root == nil {
		_root := path.Join(xdg.ConfigHome(), "pinser")
		root = &_root
	}
	filename := path.Join(*root, "config")

	_, err := os.Stat(filename)
	if err != nil {
		return nil, err
	}
	file, err := ini.LoadSources(ini.LoadOptions{
		KeyValueDelimiters: "=",
	}, filename)
	if err != nil {
		return nil, err
	}

	conf := &PinserConfig{
		Ini: file,
	}
	conf.LoadConfig(file)
	return conf, nil
}
